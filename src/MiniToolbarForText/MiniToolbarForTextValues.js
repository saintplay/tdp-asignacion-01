export const FONT_VALUES = [
  "Arial",
  "PT Mono",
  "Comic Neue",
  "DejaVu Sans Mono",
  "Fantasque Sans Mono",
  "Source Code Pro",
  "Ubuntu Mono"
];

export const FONT_SIZE_VALUES = [
  7,
  8,
  9,
  10,
  11,
  12,
  14,
  16,
  18,
  20,
  24,
  28,
  32,
  40,
  64
];

export const ALIGN_KEYS = {
  left: "LEFT",
  center: "CENTER",
  right: "RIGHT"
};

export const COLOR_VALUES = [
  "rgb(255, 255, 255)",
  "rgb(152, 0, 0)",
  "rgb(255, 0, 0)",
  "rgb(255, 153, 0)",
  "rgb(255, 255, 0)",
  "rgb(0, 255, 0)",
  "rgb(0, 255, 255)",
  "rgb(74, 134, 232)",
  "rgb(0, 0, 255)",
  "rgb(153, 0, 255)",
  "rgb(255, 0, 255)",
  "rgb(0, 0, 0)"
];

export const DEFAULT_FONT_VALUE = "Arial";
export const DEFAULT_FONT_SIZE_VALUE = 11;
export const DEFAULT_COLOR_VALUE = "rgb(255, 255, 255)";
