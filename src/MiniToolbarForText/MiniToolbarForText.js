import React from "react";

import DropDownMenu from "react-uwp/DropDownMenu";
import Flyout from "react-uwp/Flyout";
import FlyoutContent from "react-uwp/FlyoutContent";
import Button from "react-uwp/Button";
import {
  ALIGN_KEYS,
  FONT_VALUES,
  FONT_SIZE_VALUES,
  COLOR_VALUES,
  DEFAULT_COLOR_VALUE,
  DEFAULT_FONT_VALUE,
  DEFAULT_FONT_SIZE_VALUE
} from "./MiniToolbarForTextValues";

const BUTTON_BG = "rgba(255, 255, 255, 0.2)";
const BUTTON_ACTIVE_BG = "rgba(255, 255, 255, 0.4)";

const getButtonBg = condition => (condition ? BUTTON_ACTIVE_BG : BUTTON_BG);

const getAlignIcon = value => {
  switch (value) {
    case ALIGN_KEYS.right:
      return "AlignRightLegacy";
    case ALIGN_KEYS.center:
      return "AlignCenterLegacy";
    case ALIGN_KEYS.left:
    default:
      return "AlignLeftLegacy";
  }
};

const callPropEvent = (propEvent, payload) => {
  if (propEvent) propEvent(payload);
};

export class MiniToolbarForText extends React.Component {
  constructor(props) {
    super(props);

    this.alignRef = React.createRef();
    this.colorRef = React.createRef();

    this.state = {
      boldValue: false,
      italicValue: false,
      underlineValue: false,
      alignValue: "left",
      fontDefaultValue: DEFAULT_FONT_VALUE,
      fontSizeDefaultValue: DEFAULT_FONT_SIZE_VALUE,
      colorValue: DEFAULT_COLOR_VALUE
    };
  }

  onControlClick(property, newValue, propEvent) {
    this.setState({ [property]: newValue });
    callPropEvent(propEvent, newValue);
  }

  render() {
    return (
      <div>
        <div>
          <DropDownMenu
            values={FONT_VALUES}
            defaultValue={this.state.fontDefaultValue}
            itemWidth={143}
            onChangeValue={value => {
              callPropEvent(this.props.onFontChange, value);
              this.setState({ fontDefaultValue: value });
            }}
          />
          <DropDownMenu
            values={FONT_SIZE_VALUES}
            defaultValue={this.state.fontSizeDefaultValue}
            onChangeValue={value => {
              callPropEvent(this.props.onFontSizeChange, value);
              this.setState({ fontSizeDefaultValue: value });
            }}
          />
        </div>
        <div>
          <Button
            icon="BoldLegacy"
            background={getButtonBg(this.state.boldValue)}
            onClick={() =>
              this.onControlClick(
                "boldValue",
                !this.state.boldValue,
                this.props.onBoldChange
              )
            }
          />
          <Button
            icon="ItalicLegacy"
            background={getButtonBg(this.state.italicValue)}
            onClick={() =>
              this.onControlClick(
                "italicValue",
                !this.state.italicValue,
                this.props.onItalicChange
              )
            }
          />
          <Button
            icon="UnderlineLegacy"
            background={getButtonBg(this.state.underlineValue)}
            onClick={() =>
              this.onControlClick(
                "underlineValue",
                !this.state.underlineValue,
                this.props.onUnderlineChange
              )
            }
          />
          <Flyout>
            <Button icon={getAlignIcon(this.state.alignValue)} />
            <FlyoutContent
              ref={this.alignRef}
              show={false}
              verticalPosition="bottom"
              margin={0}
              style={{ padding: 0, width: 165 }}
            >
              <div>
                <Button
                  icon="AlignLeftLegacy"
                  onClick={() => {
                    this.alignRef.current.hideFlyoutContent();
                    this.onControlClick(
                      "alignValue",
                      ALIGN_KEYS.left,
                      this.props.onAlignChange
                    );
                  }}
                />
                <Button
                  icon="AlignCenterLegacy"
                  onClick={() => {
                    this.alignRef.current.hideFlyoutContent();
                    this.onControlClick(
                      "alignValue",
                      ALIGN_KEYS.center,
                      this.props.onAlignChange
                    );
                  }}
                />
                <Button
                  icon="AlignRightLegacy"
                  onClick={() => {
                    this.alignRef.current.hideFlyoutContent();
                    this.onControlClick(
                      "alignValue",
                      ALIGN_KEYS.right,
                      this.props.onAlignChange
                    );
                  }}
                />
              </div>
            </FlyoutContent>
          </Flyout>

          <Flyout>
            <Button icon="ColorLegacy" />
            <FlyoutContent
              ref={this.colorRef}
              show={false}
              horizontalPosition="left"
              verticalPosition="bottom"
              margin={0}
              style={{ padding: 8, width: 240 }}
            >
              <div>
                {COLOR_VALUES.map(color => (
                  <Button
                    key={color}
                    style={{ padding: 0 }}
                    onClick={() => {
                      this.colorRef.current.hideFlyoutContent();
                      this.onControlClick(
                        "colorValue",
                        color,
                        this.props.onColorChange
                      );
                    }}
                  >
                    <div
                      style={{
                        width: 16,
                        height: 16,
                        backgroundColor: color
                      }}
                    />
                  </Button>
                ))}
              </div>
            </FlyoutContent>
          </Flyout>
        </div>
      </div>
    );
  }
}
