import React, { Component } from "react";

import Button from "react-uwp/Button";
import TextBox from "react-uwp/TextBox";

import HeightIcon from "./HeightIcon";
import WidthIcon from "./WidthIcon";

const callPropEvent = (propEvent, payload) => {
  if (propEvent) propEvent(payload);
};

export class MiniToolbarForImage extends Component {
  onControlClick(property, newValue, propEvent) {
    this.setState({ [property]: newValue });
    callPropEvent(propEvent, newValue);
  }

  render() {
    return (
      <div>
        <Button
          icon="CropLegacy"
          style={{ height: "32px" }}
          onClick={() =>
            this.onControlClick(
              "boldValue",
              !this.state.boldValue,
              this.props.onBoldChange
            )
          }
        />
        <div
          style={{
            display: "inline-block",
            padding: "0 10px",
            color: "rgba(255, 255, 2555, 0.6)"
          }}
        >
          <WidthIcon style={{ transform: "translateY(4px)" }} />
          <span style={{ paddingLeft: "10px" }}>Width</span>
        </div>
        <TextBox style={{ display: "inline-flex", width: "60px" }} />
        <div
          style={{
            display: "inline-block",
            padding: "0 10px",
            color: "rgba(255, 255, 2555, 0.6)"
          }}
        >
          <HeightIcon style={{ transform: "translateY(4px)" }} />
          <span style={{ paddingLeft: "10px" }}>Height</span>
        </div>
        <TextBox style={{ display: "inline-flex", width: "60px" }} />
      </div>
    );
  }
}
