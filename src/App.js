import React, { Component } from "react";
import "./App.css";

import { Theme, getTheme } from "react-uwp/Theme";
import { MiniToolbarForText } from "./MiniToolbarForText/MiniToolbarForText";
import { MiniToolbarForImage } from "./MiniToolbarForImage/MiniToolbarForImage";

class App extends Component {
  render() {
    return (
      <Theme
        theme={getTheme({
          themeName: "dark"
        })}
      >
        <div className="app">
          <MiniToolbarForText
            onFontChange={newValue => console.log("Font changed to", newValue)}
            onFontSizeChange={newValue =>
              console.log("Font Size changed to", newValue)
            }
            onBoldChange={newValue => console.log("Bold changed to", newValue)}
            onItalicChange={newValue =>
              console.log("Italic changed to", newValue)
            }
            onUnderlineChange={newValue =>
              console.log("Underline changed to", newValue)
            }
            onAlignChange={newValue =>
              console.log("Align changed to", newValue)
            }
            onColorChange={newValue =>
              console.log("Color changed to", newValue)
            }
          />

          <div style={{ marginTop: "20px", marginBottom: "20px" }}>
            <hr />
          </div>

          <MiniToolbarForImage />
        </div>
      </Theme>
    );
  }
}

export default App;
